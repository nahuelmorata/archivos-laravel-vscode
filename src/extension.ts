import * as vscode from 'vscode';

import './dataProviders/modelosDataProvider';
import { ModelosClassProvider } from './dataProviders/modelosDataProvider';
import { ConfigClassProvider } from './dataProviders/configDataProvider';
import { GraphqlClassProvider } from './dataProviders/graphqlDataProvider';
import { DBClassProvider } from './dataProviders/dbDataProvider';

export function activate(context: vscode.ExtensionContext) {
    vscode.window.registerTreeDataProvider('modelos-laravel', new ModelosClassProvider());
    vscode.window.registerTreeDataProvider('config-laravel', new ConfigClassProvider());
    vscode.window.registerTreeDataProvider('graphql-laravel', new GraphqlClassProvider());
    vscode.window.registerTreeDataProvider('database-laravel', new DBClassProvider());

    let disposableAbrirArchivo = vscode.commands.registerCommand('extension.abrirArchivo', (path) => {
        vscode.window.showTextDocument(vscode.Uri.file(path));
    });

	context.subscriptions.push(disposableAbrirArchivo);
}

export function deactivate() {}
