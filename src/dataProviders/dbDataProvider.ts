import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';

import { DataItem } from './dataItem';
import { pathExists } from '../utils';

export class DBClassProvider implements vscode.TreeDataProvider<DataItem> {
    private _workspaceRoot: string = "";

    constructor() {
        if (vscode.workspace.workspaceFolders !== undefined) {
            this._workspaceRoot = vscode.workspace.workspaceFolders[0].uri.path;
        }
    }

    getTreeItem(element: DataItem): vscode.TreeItem {
        return element;
    }

    getChildren(element?: DataItem | undefined): vscode.ProviderResult<DataItem[]> {
        if (!this._workspaceRoot) {
            vscode.window.showInformationMessage("Primero abra una carpeta");
            return Promise.resolve([]);
        }

        if (!element) {
            return Promise.resolve(this.cargarDatabase());
        } else {
            if (element.label !== undefined) {
                return Promise.resolve(this.cargar(element.label));
            }
            return Promise.resolve([]);
        }
    }
    
    private cargarDatabase(): DataItem[] {
        const dataItems: DataItem[] = new Array<DataItem>();
        const carpetaDatabase: string = path.join(this._workspaceRoot, "database");

        if (pathExists(carpetaDatabase)) {
            const listDir: string[] = fs.readdirSync(carpetaDatabase);

            listDir.forEach((archivo: string) => {
                if (fs.lstatSync(path.join(carpetaDatabase, archivo)).isDirectory()) {
                    const listDir: string[] = fs.readdirSync(path.join(carpetaDatabase, archivo));

                    dataItems.push(new DataItem(archivo, "", vscode.TreeItemCollapsibleState.Collapsed));
                }
            });
        }

        return dataItems;
    }

    private cargar(carpeta: string): DataItem[] {
        const dataItems: DataItem[] = new Array<DataItem>();
        const carpetaElemento: string = path.join(this._workspaceRoot, "database", carpeta);

        if (pathExists(carpetaElemento)) {
            const listDir: string[] = fs.readdirSync(carpetaElemento);

            listDir.forEach((archivo: string) => {
                if (fs.lstatSync(path.join(carpetaElemento, archivo)).isFile()) {
                    let nombre;
                    if (carpeta === "migrations") {
                        let indexUltimoGuion = archivo.lastIndexOf('_');
                        let indexAnteUltimoGuion = archivo.substring(0, indexUltimoGuion - 1).lastIndexOf('_');

                        nombre = archivo.substring(indexUltimoGuion, indexAnteUltimoGuion + 1);
                    } else {
                        nombre = archivo.substring(0, archivo.length - 4);
                    }

                    const pathArchivo = path.join(carpetaElemento, archivo);

                    dataItems.push(new DataItem(nombre, carpeta, vscode.TreeItemCollapsibleState.None, {
                        command: 'extension.abrirArchivo',
                        arguments: [pathArchivo],
                        title: 'Abrir'
                    }));
                }
            });
        }

        return dataItems;
    }
}