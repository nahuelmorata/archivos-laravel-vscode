import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';

import { DataItem } from './dataItem';
import { pathExists } from '../utils';

export class ConfigClassProvider implements vscode.TreeDataProvider<DataItem> {
    private _workspaceRoot: string = "";

    constructor() {
        if (vscode.workspace.workspaceFolders !== undefined) {
            this._workspaceRoot = vscode.workspace.workspaceFolders[0].uri.path;
        }
    }

    getTreeItem(element: DataItem): vscode.TreeItem {
        return element;
    }

    getChildren(element?: DataItem | undefined): vscode.ProviderResult<DataItem[]> {
        if (!this._workspaceRoot) {
            vscode.window.showInformationMessage("Primero abra una carpeta");
            return Promise.resolve([]);
        }

        if (!element) {
            return Promise.resolve(this.cargarConfigs());
        }

        return Promise.resolve([]);
    }
    
    private cargarConfigs(): DataItem[] {
        const archivosData: DataItem[] = new Array<DataItem>();
        const carpetaConfig: string = path.join(this._workspaceRoot, "config");

        if (pathExists(carpetaConfig)) {
            const listDir: string[] = fs.readdirSync(carpetaConfig);
            const configs: string[] | undefined = vscode.workspace.getConfiguration("archivoslaravel").get('filtroConfig');

            listDir.forEach((archivo: string) => {
                if (archivo.substring(archivo.length - 3) === "php") {
                    const nombre = archivo.substring(0, archivo.length - 4);
                    const pathArchivo = path.join(carpetaConfig, archivo);

                    if (configs !== undefined) {
                        if (configs.includes(nombre)) {
                            archivosData.push(new DataItem(nombre, "Configuracion:", vscode.TreeItemCollapsibleState.None, {
                                command: 'extension.abrirArchivo',
                                arguments: [pathArchivo],
                                title: 'Abrir'
                            }));
                        }
                    } else {
                        archivosData.push(new DataItem(nombre, "Configuracion:", vscode.TreeItemCollapsibleState.None, {
                            command: 'extension.abrirArchivo',
                            arguments: [pathArchivo],
                            title: 'Abrir'
                        }));
                    }
                }
            });
        }

        return archivosData;
    }
}