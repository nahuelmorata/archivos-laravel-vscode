import * as vscode from 'vscode';

export class DataItem extends vscode.TreeItem {
    private _tipo: string;

    constructor(label: string, tipo: string, collapsibleState: vscode.TreeItemCollapsibleState, command?: vscode.Command) {
        super(label, collapsibleState);

        this.command = command;
        this._tipo = tipo;
    }

    get tooltip(): string {
        return `${this._tipo} ${this.label}`;
    }
}