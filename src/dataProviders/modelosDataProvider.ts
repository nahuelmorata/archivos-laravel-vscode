import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';

import { DataItem } from './dataItem';
import { pathExists } from '../utils';

export class ModelosClassProvider implements vscode.TreeDataProvider<DataItem> {
    private _workspaceRoot = "";

    constructor() {
        if (vscode.workspace.workspaceFolders !== undefined) {
            this._workspaceRoot = vscode.workspace.workspaceFolders[0].uri.path;
        }
    }

    getTreeItem(element: DataItem): vscode.TreeItem {
        return element;
    }

    getChildren(element?: DataItem | undefined): vscode.ProviderResult<DataItem[]> {
        if (!this._workspaceRoot) {
            vscode.window.showInformationMessage("Primero abra una carpeta");
            return Promise.resolve([]);
        }

        if (!element) {
            return Promise.resolve(this.leerModelos());
        } 

        return Promise.resolve([]);
    }

    private leerModelos(): DataItem[] { 
        const carpetaApp = path.join(this._workspaceRoot, 'app');
        const modelosData: DataItem[] = new Array<DataItem>();

        if (pathExists(carpetaApp)) {
            const listDir: string[] = fs.readdirSync(carpetaApp);

            listDir.forEach((archivo: string) => {
                if (fs.lstatSync(path.join(carpetaApp, archivo)).isFile()) {
                    if (archivo.substring(archivo.length - 3) === "php") {
                        const pathArchivo = path.join(carpetaApp, archivo);
                        const nombre = archivo.substring(0, archivo.length - 4);

                        modelosData.push(new DataItem(nombre, "Modelo:", vscode.TreeItemCollapsibleState.None, {
                            command: 'extension.abrirArchivo',
                            arguments: [pathArchivo],
                            title: 'Abrir'
                        }));
                    }
                }
            });
        }

        return modelosData;
    }
}

