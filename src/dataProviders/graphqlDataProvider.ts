import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';

import { DataItem } from './dataItem';
import { pathExists } from '../utils';

export class GraphqlClassProvider implements vscode.TreeDataProvider<DataItem> {
    private _workspaceRoot: string = "";

    constructor() {
        if (vscode.workspace.workspaceFolders !== undefined) {
            this._workspaceRoot = vscode.workspace.workspaceFolders[0].uri.path;
        }
    }

    getTreeItem(element: DataItem): vscode.TreeItem {
        return element;
    }

    getChildren(element?: DataItem | undefined): vscode.ProviderResult<DataItem[]> {
        if (!this._workspaceRoot) {
            vscode.window.showInformationMessage("Primero abra una carpeta");
            return Promise.resolve([]);
        }

        if (!element) {
            return Promise.resolve(this.cargarGraphql());
        } else {
            if (element.label !== undefined) {
                return Promise.resolve(this.cargar(element.label));
            }
            return Promise.resolve([]);
        }
    }
    
    private cargarGraphql(): DataItem[] {
        const dataItems: DataItem[] = new Array<DataItem>();
        const carpetaGraphql: string = path.join(this._workspaceRoot, "app", "GraphQL");

        if (pathExists(carpetaGraphql)) {
            const listDir: string[] = fs.readdirSync(carpetaGraphql);

            listDir.forEach((archivo: string) => {
                if (fs.lstatSync(path.join(carpetaGraphql, archivo)).isDirectory()) {
                    const listDir: string[] = fs.readdirSync(path.join(carpetaGraphql, archivo));

                    dataItems.push(new DataItem(archivo, "", vscode.TreeItemCollapsibleState.Collapsed));
                }
            });
        }

        return dataItems;
    }

    private cargar(carpeta: string): DataItem[] {
        const dataItems: DataItem[] = new Array<DataItem>();
        const carpetaElemento: string = path.join(this._workspaceRoot, "app", "GraphQL", carpeta);

        if (pathExists(carpetaElemento)) {
            const listDir: string[] = fs.readdirSync(carpetaElemento);

            listDir.forEach((archivo: string) => {
                if (fs.lstatSync(path.join(carpetaElemento, archivo)).isFile()) {
                    const nombre = archivo.substring(0, archivo.length - 4);
                    const pathArchivo = path.join(carpetaElemento, archivo);

                    dataItems.push(new DataItem(nombre, carpeta, vscode.TreeItemCollapsibleState.None, {
                        command: 'extension.abrirArchivo',
                        arguments: [pathArchivo],
                        title: 'Abrir'
                    }));
                }
            });
        }

        return dataItems;
    }
}