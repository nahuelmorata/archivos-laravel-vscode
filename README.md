# README de archivos-laravel 

Extension que clasifica los archivos de laravel para acceder mas rapido

## Funciones

Separa los modelos, graphql, database y config en distintas categorias

## Configuracion

Esta extension tiene como configuracion:

* `archivoslaravel.filtroConfig`: Archivos que muestra de la carpeta config. Si es vacio muestra todo

## Notas de lanzamiento

Para proximas versiones extension mas configurable

### 1.0.0

Primera version